

inputName = document.getElementById("name");
inputMail = document.getElementById("email");
inputPhone = document.getElementById("phone");


inputName.addEventListener('input',validationName);
inputMail.addEventListener('input',validationMail);
inputPhone.addEventListener('input',validationPhone);

inputName.addEventListener('focusout',validationName);
inputMail.addEventListener('focusout',validationMail);
inputPhone.addEventListener('focusout',validationPhone);

document.addEventListener('submit', valider);

//expression régulière pour le téléphone
var regPhone = new RegExp(/^0[1-9]([-. ]?[0-9]{2}){4}$/);

//expression régulière pour le Mail
var regMail = new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,}$/);



function valider(e) {
    
if (inputName.value=="" || inputName.value.length>50 ) {
    inputName.style.borderColor= "red";
    inputName.parentNode.querySelector('p').innerText="Le nom n'est pas correcte";
    e.preventDefault();// l'ation par defaut ne doit pas être executer
}

if (inputMail.value=="" || !regMail.test(inputMail.value)) {
    inputMail.style.borderColor= "red";
    inputMail.parentNode.querySelector('p').innerText="Le mail n'est pas correcte";
    e.preventDefault();
}


if (inputPhone.value=="" || !regPhone.test(inputPhone.value)) {
    inputPhone.style.borderColor= "red";
    inputPhone.parentNode.querySelector('p').innerText="Le telephone n'est pas correcte";
    e.preventDefault();
}

   
}

//verification de la validité du mail
function validationMail() {
    if (this.value=="" || !regMail.test(this.value)) {
        this.style.borderColor= "red";
        this.parentNode.querySelector('p').innerText="Veuillez remplir le champ";
  
    }
    else{
        this.style.borderColor= "green";
        this.parentNode.querySelector('p').innerText="";
    }
  
}

//verification de la longueur
function validationName() {
    if (this.value=="" || this.value.length>50 )
    {
        this.style.borderColor= "red";
        this.parentNode.querySelector('p').innerText="Champ incorecte";
    }
    else{
        this.style.borderColor= "green";
        this.parentNode.querySelector('p').innerText="";
    }
}

//verification du numero de telphone
function validationPhone() {

    if (this.value=="" || !regPhone.test(this.value))
    {
        this.style.borderColor= "red";
        this.parentNode.querySelector('p').innerText="Champ incorecte";
    }
    else{
        this.style.borderColor= "green";
        this.parentNode.querySelector('p').innerText="";
    }
}




